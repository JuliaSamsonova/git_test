package adapter;

import org.apache.commons.lang3.RandomUtils;

public class Target {
    int weight = 52;
    double length = 174.5;

    public void request() {
        System.out.println("Called Target request()");
    }

    public void calculate() {
        System.out.println("Weight index equal to" + " " + calculatedBodyIndex());
    }

    public int calculatedBodyIndex() {
        int index = (weight * (int) length) / 100;
        return index;
    }

    public void showChanges() {
        System.out.println(generateInt());
    }

    public int generateInt() {
        return RandomUtils.nextInt();
    }
}
