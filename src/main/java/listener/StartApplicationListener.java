package listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class StartApplicationListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent event) {
        System.out.println("Application start!");
    }

    public void contextDestroyed(ServletContextEvent event) {
        System.out.println("Application finished!");
    }
}
