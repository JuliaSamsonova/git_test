package filter;

import javax.servlet.*;
import java.io.IOException;

public class CharsetFilter implements Filter {
    private ServletContext context;
    private String encoding;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        encoding = filterConfig.getInitParameter("UTF-8");
        context = filterConfig.getServletContext();
        this.context.log("CharsetFilter was started");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletRequest.setCharacterEncoding(encoding);
        servletResponse.setCharacterEncoding(encoding);
        filterChain.doFilter(servletRequest, servletResponse);
        context.log("CharsetFilter was initialized");
    }

    @Override
    public void destroy() {

    }
}
